"""speedometer class for PwnBot"""


class Speedometer(object):
    """naieve speedometer"""

    def __init__(self, track):
        self.track = track
        self.prev_position = 0
        self.prev_piece = 0
        self.speed = 0
        self.prev_speed = 0

    def reset(self):
        """reset speedometer"""
        self.prev_position = 0
        self.prev_piece = 0
        self.speed = 0

    def update(self, new_piece, new_position):
        """process tick, update speed"""
        if new_piece != self.prev_piece:
            self.prev_piece = new_piece
        else:
            self.prev_speed = self.speed
            self.speed = new_position - self.prev_position

        self.prev_position = new_position
        return (self.speed, self.speed - self.prev_speed)
