"""Encapsulate Track data and related functions"""
import math


class Track:
    """Track data object"""

    def __init__(self, track):
        self.track = track['pieces']
        self.length = len(self.track)
        self.name = track['name']
        self.track_id = track['id']
        self.process_track()

    def process_track(self):
        """precompute length of curve pieces"""

        for i in self.track:
            try:
                i.setdefault('length',
                             math.pi * i['radius'] * abs(i['angle']) / 180.0)
            except KeyError:
                pass

    def next_piece(self, index):
        """returns the piece one ahead of index"""
        return self.track[(index + 1) % self.length]

    def turn_speed(self, index):
        """calculate cruising speed for turn piece at index"""
        if self.name == 'Germany':
            return 4.82
        elif self.name == 'Keimola':
            return 6.5
        elif self.name == 'USA':
            return 30.0
        else:
            return 5.0

    def next_turn_distance(self, car_position, car_index):
        """convenence function calculates distance to next turn"""
        return self.distance_to_turn(car_position, car_index,
                                     self.next_turn(car_index))

    def distance_to_turn(self, car_position, car_index, turn_index):
        """calculate distance to given turn"""
        turn_index = self.next_turn(car_index)
        distance = 0
        distance += self.track[car_index]['length'] - car_position
        for i in xrange(car_index, turn_index):
            distance += self.track[i % self.length]['length']
        return distance

    def next_turn(self, index):
        """finds the index of the next turn, from piece index"""
        for i in range(1, self.length):
            if 'angle' in self.next_piece(index+i):
                return i
        assert False  # dead code (no drag racing)
