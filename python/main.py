"""Hello World Open 2014 Entry

   "Usage: ./run host port botname botkey"
   """

import json
import socket
import sys
from speedometer import Speedometer
from track import Track


class SlotCar(object):
    """Container for slotcar attributes"""
    def __init__(self, color, turbo, turbo_factor):
        self.color = color
        self.turbo = turbo
        self.turbo_factor = turbo_factor
        self.turbo_on = False


class PwnBot(object):
    """Slot Car AI Racer"""

    MAX_SPEED = 400

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.track = None
        self.speedometer = None
        self.car = SlotCar('plaid', 0, 1.0)

    def msg(self, msg_type, data):
        """send json encoded response to race server"""
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        """write to socket"""
        self.socket.sendall(msg + "\n")

    def join(self):
        """join a race (for CI / competition)"""
        return self.msg("join", {"name": self.name, "key": self.key})

    def join_race(self):
        """join a race"""
        return self.msg("joinRace", {"botId": {"name": self.name,
                                               "key": self.key},
                                     "trackName": "keimola",
                                     "carCount": 1})

    def throttle(self, throttle):
        """send a valid throttle response message"""
        if throttle < 0.0:
            throttle = 0
        if throttle > 1.0 and self.car.turbo > 0:
            # if we're asked to drive faster than possible
            self.turbo()  # do so
        throttle = min(throttle, 1.0) / self.car.turbo_factor
        self.msg("throttle", throttle)

    def switch(self, direction):
        """switch lanes"""
        self.msg("switchLane", direction)

    def turbo(self):
        """turn on turbo booster"""
        self.car.turbo_on = True
        catchphrase = "Shit, I'm late for racing school!"
        print catchphrase
        self.msg("turbo", catchphrase)

    def ping(self):
        """send a noop msg"""
        self.msg("ping", {})

    def run(self):
        """not entirely sure why main doesn't just do this itself"""
        self.join()
        self.msg_loop()

    def on_join(self, data):
        """process join response"""
        self.ping()

    def on_game_init(self, data):
        """process gameinit reponse, which includes track data"""
        print "Game initialized"
        self.track = Track(data['race']['track'])
        self.speedometer = Speedometer(self.track)

    def on_game_start(self, data):
        """process game has started message, floor it!"""
        print("Race started %s" % data)
        self.throttle(1.0)

    def on_car_positions(self, data):
        """process game tick; heavy lifting is done here"""
        car = data[0]
        pos = car['piecePosition']
        (speed, accel) = self.speedometer.update(pos['pieceIndex'],
                                                 pos['inPieceDistance'])
        if self.car.turbo_on:
            self.car.turbo -= 1
            self.car.turbo_on = self.car.turbo == 0
        dist = self.track.next_turn_distance(pos['inPieceDistance'],
                                             pos['pieceIndex'])
        if dist > 200:
            print "distance: %s" % dist
            target_speed = self.MAX_SPEED
        else:
            target_speed = self.track.turn_speed(0)

        error = (target_speed - speed)
        correction = error/10.0

        # more optimal using piecewise linear
        if error > .15:
            correction = 3.00
        if error < -0.15:
            correction = -1.0

        print "speed: %s" % speed

        self.throttle(target_speed/10.0+correction)

    def on_crash(self, data):
        """process crash msg"""
        if data['color'] == self.car.color:
            print("I crashed")
        else:
            print("Opponent %s crashed!" % data['color'])
        self.ping()

    def on_spawn(self, data):
        """process respawn msg"""
        if data['color'] == self.car.color:
            print("I respawned")
            self.throttle(1.0)

    def on_game_end(self, data):
        """process race over msg, reset vars"""
        self.ping()

    def on_lapfinished(self, data):
        """process lapfinish msg"""
        if data['car']['color'] == self.car.color:
            print "laptime: %s" % data['lapTime']['millis']

    def on_error(self, data):
        """display errors responses"""
        print("Error: {0}".format(data))
        self.ping()

    def on_turbo_available(self, data):
        """Turbo multiplies the power of your engine for a short period."""
        self.car.turbo = data['turboDurationTicks']
        self.car.turbo_factor = data['turboFactor']

    def on_turbo_end(self, data):
        """reset car physics on turbo_end"""
        self.car.turbo = 0
        self.car.turbo_factor = 1.0

    def on_your_car(self, data):
        """store self identifier"""
        self.car.color = data['color']
        print("Playing as %s" % self.car.color)

    def msg_loop(self):
        """main event processing loop. Twisted, anyone?"""
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'turboAvailable': self.on_turbo_available,
            'turboEnd': self.on_turbo_end,
            'lapFinished': self.on_lapfinished,
            'yourCar': self.on_your_car,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = SOCK.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print "Usage: ./run host port botname botkey"
    else:
        HOST, PORT, NAME, KEY = sys.argv[1:5]
        print "Connecting..."
        print "{0}:{1}, bot name={2}, key={3}".format(*sys.argv[1:5])
        SOCK = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        SOCK.connect((HOST, int(PORT)))
        BOT = PwnBot(SOCK, NAME, KEY)
        BOT.run()
